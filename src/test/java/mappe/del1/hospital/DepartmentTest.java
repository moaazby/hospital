package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import mappe.del1.hospital.healthpersonal.Nurse;
import mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import mappe.del1.hospital.healthpersonal.doctor.Surgeon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {
    private Hospital hospital = new Hospital("St. Olavs");

    @Nested
    class RemovePersonAndCheckListSize{

        @Test
        @DisplayName("Remove an employee and check the size for employees list after removing")
        void RemoveAnEmployeeAndCheckListSizeBeforeAndAfter() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);
            int beforeRemove = hospital.getDepartments().get("Akutten").getEmployees().size();

            Employee employeeToRemove = new Employee("Odd Even", "Primtallet", "111111");
            hospital.getDepartments().get("Akutten").removePerson(employeeToRemove);

            int afterRemove = hospital.getDepartments().get("Akutten").getEmployees().size();

            assertEquals(beforeRemove-1,afterRemove,"This method check the size of employees list before and after removing an employee");

        }


        @Test
        @DisplayName("Remove a general practitioner and check the size for employees list after removing")
        void RemoveAGeneralPractitionerAndCheckListSizeBeforeAndAfter() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);
            int beforeRemove = hospital.getDepartments().get("Akutten").getEmployees().size();

            GeneralPractitioner generalPractitionerToRemove = new GeneralPractitioner("Inco", "Gnito", "444444");
            hospital.getDepartments().get("Akutten").removePerson(generalPractitionerToRemove);

            int afterRemove = hospital.getDepartments().get("Akutten").getEmployees().size();

            assertEquals(beforeRemove-1,afterRemove,"This method check the size of employees list before and after removing a general practitioner");

        }

        @Test
        @DisplayName("Remove a surgeon and check the size for employees list after removing")
        void RemoveASurgeonAndCheckListSizeBeforeAndAfter() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);
            int beforeRemove = hospital.getDepartments().get("Akutten").getEmployees().size();

            Surgeon surgeonToRemove = new Surgeon("Inco", "Gnito", "555555");
            hospital.getDepartments().get("Akutten").removePerson(surgeonToRemove);

            int afterRemove = hospital.getDepartments().get("Akutten").getEmployees().size();

            assertEquals(beforeRemove-1,afterRemove,"This method check the size of employees list before and after removing a surgeon");

        }

        @Test
        @DisplayName("Remove a nurse and check the size for employees list after removing")
        void RemoveANurseAndCheckListSizeBeforeAndAfter() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);
            int beforeRemove = hospital.getDepartments().get("Akutten").getEmployees().size();

            Nurse nurseToRemove = new Nurse("Nina", "Teknologi", "666666");
            hospital.getDepartments().get("Akutten").removePerson(nurseToRemove);

            int afterRemove = hospital.getDepartments().get("Akutten").getEmployees().size();

            assertEquals(beforeRemove-1,afterRemove,"This method check the size of employees list before and after removing a nurse");

        }



        @Test
        @DisplayName("Remove a patient and check the size for patients list after removing")
        void RemoveAPatientAndCheckListSizeBeforeAndAfter() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);
            int beforeRemove = hospital.getDepartments().get("Akutten").getPatients().size();

            Patient patientToRemove = new Patient("Inga", "Lykke", "888888");
            hospital.getDepartments().get("Akutten").removePerson(patientToRemove);

            int afterRemove = hospital.getDepartments().get("Akutten").getPatients().size();

            assertEquals(beforeRemove-1,afterRemove,"This method check the size of patients list before and after removing a patient");

        }

    }

    @Nested
    class RemovePersonAndCheckIfListContainsThisPerson{

        @Test
        @DisplayName("Remove an employee and check if this employee is in the list or not after removing")
        void RemoveAnEmployeeAndCheckIfExistInTheList() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);

            Employee employeeToRemove = new Employee("Odd Even", "Primtallet", "111111");
            hospital.getDepartments().get("Akutten").removePerson(employeeToRemove);

            assertFalse(hospital.getDepartments().get("Akutten").getEmployees().containsKey("111111"));

        }

        @Test
        @DisplayName("Remove a nurse and check if this employee is in the list or not after removing")
        void RemoveANurseAndCheckIfExistInTheList() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);

            Nurse nurseToRemove = new Nurse("Nina", "Teknologi", "666666");
            hospital.getDepartments().get("Akutten").removePerson(nurseToRemove);

            assertFalse(hospital.getDepartments().get("Akutten").getEmployees().containsKey("666666"));

        }

        @Test
        @DisplayName("Remove a general practitioner and check if this employee is in the list or not after removing")
        void RemoveAGeneralPractitionerAndCheckIfExistInTheList() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);

            GeneralPractitioner generalPractitionerToRemove = new GeneralPractitioner("Inco", "Gnito", "444444");
            hospital.getDepartments().get("Akutten").removePerson(generalPractitionerToRemove);

            assertFalse(hospital.getDepartments().get("Akutten").getEmployees().containsKey("444444"));

        }

        @Test
        @DisplayName("Remove a surgeon and check if this employee is in the list or not after removing")
        void RemoveASurgeonAndCheckIfExistInTheList() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);

            Surgeon surgeonToRemove = new Surgeon("Inco", "Gnito", "555555");
            hospital.getDepartments().get("Akutten").removePerson(surgeonToRemove);

            assertFalse(hospital.getDepartments().get("Akutten").getEmployees().containsKey("555555"));

        }

        @Test
        @DisplayName("Remove a patient and check if this patient is in the list or not after removing")
        void RemoveAPatientAndCheckIfExistInTheList() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);

            Patient patientToRemove = new Patient("Inga", "Lykke", "888888");
            hospital.getDepartments().get("Akutten").removePerson(patientToRemove);

            assertFalse(hospital.getDepartments().get("Akutten").getEmployees().containsKey("888888"));

        }
    }

    @Nested
    class PersonIsNotRemoved{

        @Test
        @DisplayName("Employee Not found, throw RemoveException")
        void employeeNotFound() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);
            Employee employeeToRemove = new Employee("Moe", "Smål", "6325146");

            assertThrows(RemoveException.class, ()-> hospital.getDepartments().get("Akutten").removePerson(employeeToRemove),"Removing a non existent employee should throw RemoveException");

        }

        @Test
        @DisplayName("Employee 'Nurse' Not found, throw RemoveException")
        void NurseNotFound() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);
            Nurse nurseToRemove = new Nurse("Moe", "Smål", "3625454");

            assertThrows(RemoveException.class, ()-> hospital.getDepartments().get("Akutten").removePerson(nurseToRemove),"Removing a non existent nurse should throw RemoveException");

        }

        @Test
        @DisplayName("Employee 'General practitioner' Not found, throw RemoveException")
        void GeneralPractitionerNotFound() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);
            GeneralPractitioner generalPractitionerToRemove = new GeneralPractitioner("Moe", "Smål", "3625454");

            assertThrows(RemoveException.class, ()-> hospital.getDepartments().get("Akutten").removePerson(generalPractitionerToRemove),"Removing a non existent general practitioner should throw RemoveException");

        }

        @Test
        @DisplayName("Employee 'Surgeon' Not found, throw RemoveException")
        void SurgeonNotFound() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);
            Surgeon surgeonToRemove = new Surgeon("Moe", "Smål", "3625454");

            assertThrows(RemoveException.class, ()-> hospital.getDepartments().get("Akutten").removePerson(surgeonToRemove),"Removing a non existent surgeon should throw RemoveException");

        }


        @Test
        @DisplayName("Patient Not found, throw RemoveException")
        void patientNotFound() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);
            Patient patientToRemove = new Patient("Moe", "Smål", "6325146");

            assertThrows(RemoveException.class, ()-> hospital.getDepartments().get("Akutten").removePerson(patientToRemove),"Removing a non existent patient should throw RemoveException");

        }


    }

    @Nested
    class RemoveManyPeopleRespectively{

        @Test
        @DisplayName("Remove many people respectively successfully, does not throw exception")
        void RemoveManyPeopleSuccessfully(){
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);

            Employee e = new Employee("Rigmor", "Mortis", "333333");
            GeneralPractitioner g = new GeneralPractitioner("Inco", "Gnito", "444444");
            Surgeon s = new Surgeon("Inco", "Gnito", "555555");
            Nurse n = new Nurse("Nina", "Teknologi", "666666");
            Patient p = new Patient("Inga", "Lykke", "888888");

            assertDoesNotThrow(()-> hospital.getDepartments().get("Akutten").removePerson(e));
            assertDoesNotThrow(()-> hospital.getDepartments().get("Akutten").removePerson(g));
            assertDoesNotThrow(()-> hospital.getDepartments().get("Akutten").removePerson(s));
            assertDoesNotThrow(()-> hospital.getDepartments().get("Akutten").removePerson(n));
            assertDoesNotThrow(()-> hospital.getDepartments().get("Akutten").removePerson(p));

        }

        @Test
        @DisplayName("Remove many people respectively, check the size for both lists(employees and patients)")
        void RemoveManyPeopleAndCheckBothLists() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);

            Employee e = new Employee("Rigmor", "Mortis", "333333");
            GeneralPractitioner g = new GeneralPractitioner("Inco", "Gnito", "444444");
            Surgeon s = new Surgeon("Inco", "Gnito", "555555");
            Nurse n = new Nurse("Nina", "Teknologi", "666666");
            Patient p = new Patient("Inga", "Lykke", "888888");

            int beforeRemove = hospital.getDepartments().get("Akutten").getEmployees().size() +
                               hospital.getDepartments().get("Akutten").getPatients().size();

            hospital.getDepartments().get("Akutten").removePerson(e);
            hospital.getDepartments().get("Akutten").removePerson(g);
            hospital.getDepartments().get("Akutten").removePerson(s);
            hospital.getDepartments().get("Akutten").removePerson(n);
            hospital.getDepartments().get("Akutten").removePerson(p);

            int afterRemove = hospital.getDepartments().get("Akutten").getEmployees().size() +
                              hospital.getDepartments().get("Akutten").getPatients().size();

            assertEquals(beforeRemove-5,afterRemove);




        }



    }

    @Nested
    class NotSupportedInput{

        @Test
        @DisplayName("The Person is null, check the exception type ")
        void PersonIsNullThrowException() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);

            assertThrows(NullPointerException.class,()->hospital.getDepartments().get("Akutten").removePerson(null),"Person is null should throw NullPointerException");

        }

        @Test
        @DisplayName("The Person is null, throw NullPointerException, check the message ")
        void PersonIsNull() throws RemoveException {
            Hospital hospital = new Hospital("St. Olavs");
            HospitalTestData.fillRegisterWithTestData(hospital);

            try{hospital.getDepartments().get("Akutten").removePerson(null); }
            catch (NullPointerException e){
                assertEquals("Person can not be null",e.getMessage());
            }


        }

    }


}