package mappe.del1.hospital;

import java.util.HashMap;

/**
 * @author M. Yanes
 * @version 1.0 2021-03-05
 */

public class Hospital {
    private final String hospitalName;
    private HashMap<String,Department> departments; //The names of the departments are the keys.


    /**
     * Constructor which creat an object of type Hospital
     *
     * @param hospitalName: Name of the hospital
     */
    public Hospital(String hospitalName){
        if (hospitalName==null || hospitalName.isBlank())
            throw new IllegalArgumentException ("The name of the hospital is required");
        this.hospitalName = hospitalName;
        this.departments = new HashMap<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * This method gets a HashMap contains all registered departments, the keys are names for the departments.
     * @return HashMap that contains all registered departments.
     */
    public HashMap<String, Department> getDepartments() {
        return departments;
    }

    /**
     * This method register a new department to departments HashMap,
     * it checks if this department exist before adding, by the name.
     * @param newDepartment: an object of type Department
     */
    public void addDepartment(Department newDepartment){
        if(departments.containsKey(newDepartment.getDepartmentName()))
            throw new IllegalArgumentException("This department exist in the hospital");
        departments.put(newDepartment.getDepartmentName(), newDepartment);
    }

    /**
     * toString method
     * @return string: name of the hospital with all registered info in all departments
     * (name of the department, all employees' info, all patients' info)
     */
    @Override
    public String toString() {
        //TODO: ok
        String departmentsDetails="";
        for(Department dep: departments.values()){
            departmentsDetails += dep.toString()+"##################################################\n";
        }

        return this.hospitalName+" Hospital\n\n" + departmentsDetails;


    }
}
