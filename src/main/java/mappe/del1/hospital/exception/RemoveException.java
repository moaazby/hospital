package mappe.del1.hospital.exception;

/**
 * This is a throwable class, will be thrown when an object are not removed.
 * It has to constructors, one with a specified message and the other take the message as an argument.
 * @author M. Yanes
 * @version 1.0 2021-03-05
 */
public class RemoveException  extends Exception{
    /**
     * Is used during deserialization to verify that the sender and receiver of a serialized object
     * have loaded classes for that object that are compatible with respect to serialization
     */
    static final long serialVersionUID = 1L;

    /**
     * This constructor take a message as argument.
     * @param msg: String
     */
    public RemoveException(String msg){
        super(msg);
    }

    /**
     * A constructor with a specified message
     */
    public RemoveException(){
        super("This person not found!");
    }
}
