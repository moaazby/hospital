package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;

/**
 * @author M. Yanes
 * @version 1.0 2021-03-05
 */
public class GeneralPractitioner extends Doctor{

    /**
     * constructor which creat an object of type GeneralPractitioner
     * @param firstName: First name
     * @param lastName: Last name
     * @param socialSecurityNumber: Social security number
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * This method is the implementation of the abstract method in Doctor class
     * @param patient: an object of type Patient
     * @param diagnosis: which the patient will have
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        if(patient==null)
            throw new IllegalArgumentException("A patient required");
        patient.setDiagnosis(diagnosis);
    }


}
