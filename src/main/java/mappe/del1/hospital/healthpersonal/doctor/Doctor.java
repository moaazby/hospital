package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Employee;
import mappe.del1.hospital.Patient;

/**
 * @author M. Yanes
 * @version 1.0 2021-03-05
 */
public abstract class Doctor extends Employee {

    /**
     * @param firstName: First name
     * @param lastName: Last name
     * @param socialSecurityNumber: social security number
     */
    protected Doctor (String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    public abstract  void setDiagnosis(Patient patient, String diagnosis);
}
