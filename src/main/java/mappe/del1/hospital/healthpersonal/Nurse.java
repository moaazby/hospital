package mappe.del1.hospital.healthpersonal;

import mappe.del1.hospital.Employee;

/**
 * @author M. Yanes
 * @version 1.0 2021-03-05
 */
public class Nurse extends Employee {

    /**
     * constructor which creat an object of type Nurse
     * @param firstName: First name
     * @param lastName: Last name
     * @param socialSecurityNumber: Social security number
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * toString method
     * @return string: a nurse personal info(social security number and full name)
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
