package mappe.del1.hospital;

/**
 * @author M. Yanes
 * @version 1.0 2021-03-05
 */
public class Employee extends Person{

    /**
     * constructor which creat an object of type Employee.
     * @param firstName: First name
     * @param lastName: Last name
     * @param socialSecurityNumber: Social security number
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    /**
     * toString method
     * @return string: an employee personal info(social security number and full name)
     */
    @Override
    public String toString() { return super.toString();
    }
}
