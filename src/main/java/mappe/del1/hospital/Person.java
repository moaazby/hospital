package mappe.del1.hospital;

/**
 * @author M. Yanes
 * @version 1.0 2021-03-05
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * @param firstName: First name
     * @param lastName: Last name
     * @param socialSecurityNumber: social security number
     * @throws IllegalArgumentException if one of the parameters is blank.
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        if(firstName==null || lastName==null || firstName.isBlank() || lastName.isBlank())
            throw new IllegalArgumentException("Full name is required");
        if(socialSecurityNumber==null || socialSecurityNumber.isBlank())
            throw new IllegalArgumentException("Social security number is required");
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Set a valid social security number
     * @param socialSecurityNumber : Social security number
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        if(socialSecurityNumber==null || socialSecurityNumber.isBlank())
            throw new IllegalArgumentException("Social security number is required");
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    /**
     * Set a valid first name
     * @param firstName : First name
     */
    public void setFirstName(String firstName) {
        if(firstName==null|| firstName.isBlank())
            throw new IllegalArgumentException("First name is required");
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    /**
     * Set a valid last name
     * @param lastName : Last name
     */
    public void setLastName(String lastName) {
        if(lastName==null|| lastName.isBlank())
            throw new IllegalArgumentException("Last name is required");
        this.lastName = lastName;
    }

    /**
     * Method to get the full name
     * @return the full name as string with format (firstName lastName).
     */
    public String getFullName(){
        return firstName+" "+lastName;
    }

    /**
     * toString method for a Person
     * @return string: all info about a person(Social security number and full name)
     */
    @Override
    public String toString() {
        return "Social Security Number: "+socialSecurityNumber +"\n"+
               "Name: "+this.getFullName();
    }
}
