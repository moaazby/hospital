package mappe.del1.hospital;

/**
 * @author M. Yanes
 * @version 1.0 2021-03-05
 */
public class Patient extends Person implements Diagnosable{
    private String diagnosis="";

    /**
     * constructor which creat an object of type Patient
     * @param firstName: First name
     * @param lastName: Last name
     * @param socialSecurityNumber: Social security number
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * This method is the implementation of Diagnosable interface
     * @param diagnosis: the patient's diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        if(diagnosis==null)
            throw new IllegalArgumentException("Diagnosis is required");
        this.diagnosis = diagnosis;
    }

    /**
     * toString method
     * @return string: a patient personal info(social security number and full name) with the diagnosis.
     */
    @Override
    public String toString() {
        return super.toString()+"\n"
              +"The Diagnosis:" + diagnosis;
    }
}
