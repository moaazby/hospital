package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;


/**
 *
 * A simple client program to test the functionality of the
 *  Hospital and underlying classes.
 * @author M.Yanes
 * @version 1.0 2021-03-05
 *
 */
public class HospitalClient {

    /**
     * main method
     * @param args
     */
    public static void main(String[] args) {
        Hospital hospital = new Hospital("St. Olav");
        HospitalTestData.fillRegisterWithTestData(hospital);

        //to check the result before and after removing
        System.out.println(hospital.getDepartments().get("Akutten").toString());
        Employee employeeToRemove = new Employee("Inco", "Gnito", "555555");
        Patient patientToRemove = new Patient("Moe", "Anderrson", "6445963");

        try{
            //should removed successfully
            hospital.getDepartments().get("Akutten").removePerson(employeeToRemove);

            //should fail and throw RemoveException
            hospital.getDepartments().get("Akutten").removePerson(patientToRemove);

        }catch (RemoveException e){
            System.out.println(e.getMessage());
        }

        //to check the result after
        System.out.println("\n"+hospital.getDepartments().get("Akutten").toString());

    }
}
