package mappe.del1.hospital;

/**
 * @author M. Yanes
 * @version 1.0 2021-03-05
 */
public interface Diagnosable {

    /**
     *
     * @param diagnosis the diagnosis which a patient will have, will be set by a doctor.
     */
    void setDiagnosis(String diagnosis);
}
