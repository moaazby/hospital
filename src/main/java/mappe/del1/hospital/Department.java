package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

import java.util.HashMap;
import java.util.Objects;

/**
 * @author M. Yanes
 * @version 1.0 2021-03-05
 */
public class Department {
    private String departmentName;
    private HashMap<String,Employee> employees; //The social security numbers for the employees are the keys.
    private HashMap<String,Patient> patients; //The social security numbers for the patients are the keys.

    /**
     * Constructor which creat an object of type Department
     *
     * @param departmentName: Name of the department.
     */
    public Department(String departmentName){
        if (departmentName==null || departmentName.isBlank())
            throw new IllegalArgumentException ("The name of the department is required");
        this.departmentName = departmentName;
        this.employees = new HashMap<>();
        this.patients = new HashMap<>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Set a name for the department.
     * The name should be valid
     * @param departmentName name of the department.
     */
    public void setDepartmentName(String departmentName) {
        if (departmentName==null || departmentName.isBlank())
            throw new IllegalArgumentException ("The name of the department is required");
        this.departmentName = departmentName;
    }

    /**
     * This method register a new employee to employees HashMap,
     * it checks if this employee exist before adding, by his/her social security number.
     * @param newEmployee: an object of type Employee
     */
    public void addEmployee(Employee newEmployee) {
        if(this.employees.containsKey(newEmployee.getSocialSecurityNumber()))
            throw new IllegalArgumentException("This employee exist already in this department");
        this.employees.put(newEmployee.getSocialSecurityNumber(), newEmployee);

    }

    /**
     * This method gets a HashMap contains all registered employees, the keys are employees' social security numbers.
     * @return HashMap that contains all registered employees.
     */
    public HashMap<String, Employee> getEmployees() {
        return employees;
    }

    /**
     * This method register a new patient to patients HashMap,
     * it checks if this patient exist before adding, by his/her social security number.
     * @param newPatient: an object of type Patient
     */
    public void addPatient(Patient newPatient) {
        if(this.patients.containsKey(newPatient.getSocialSecurityNumber()))
            throw new IllegalArgumentException("This patient exist already in this department");
        this.patients.put(newPatient.getSocialSecurityNumber(), newPatient);
    }

    /**
     * This method gets a HashMap contains all registered patients, the keys are patients' social security numbers.
     * @return HashMap that contains all registered patients.
     */
    public HashMap<String, Patient> getPatients() {
        return patients;
    }

    /**
     * equals method, using all info which a department has.
     * @param o
     * @return true or false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        if (!departmentName.equals(that.departmentName)) return false;
        if (!employees.equals(that.employees)) return false;
        return patients.equals(that.patients);
    }

    /**
     * hashCode method
     * @return integer number as hashcode.
     */
    @Override
    public int hashCode() {
        int result = departmentName.hashCode();
        result = 31 * result + employees.hashCode();
        result = 31 * result + patients.hashCode();
        return result;
    }


    /**
     * toString method
     * @return string: all registered info in a department(name of the department, all employees' info, all patients' info)
     */
    @Override
    public String toString() {
        String employeesInformation = "";
        String patientsInformation = "";
        for(Employee employee:this.employees.values())
            employeesInformation += employee.toString()+"\n---------------------------------\n";
        for(Patient patient:this.patients.values())
            patientsInformation += patient.toString()+"\n---------------------------------\n";

        return "Department: " + departmentName+"\n\n"+
                "************Employees************\n" + employeesInformation +
                "\n************Patients*************\n" + patientsInformation;
    }

    /**
     * This method removes an object of the type Employee or Patient,
     * it throw an exception if the provided person does not exist in the system(neither employees nor patients)
     * @param p
     * @throws RemoveException it is of the type "checked" and it is not handled in this method.
     */
    public void removePerson(Person p) throws RemoveException {
        //TODO:test:ok
        //check that the Person is not NULL, if null throw NullPointerException
        Objects.requireNonNull(p,"Person can not be null");

        //check first if this person not exists neither in employees nor in patients, so throw RemoveException.
        if(!(this.employees.containsKey(p.getSocialSecurityNumber())) &&
           !(this.patients.containsKey(p.getSocialSecurityNumber())))
            throw new RemoveException("Failed to remove.\n"+p.toString()+"\nNOT found in the system!");

        //if this person is an instance of Employee, remove from employees.
        //if this person is an instance of Patient, remove from patients.
        if(p instanceof Employee)
            this.employees.remove(p.getSocialSecurityNumber());
        else if(p instanceof Patient)
            this.patients.remove(p.getSocialSecurityNumber());


    }
}
