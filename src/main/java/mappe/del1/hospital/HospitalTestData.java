package mappe.del1.hospital;

import mappe.del1.hospital.healthpersonal.Nurse;
import mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import mappe.del1.hospital.healthpersonal.doctor.Surgeon;
public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }
    /**
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("Odd Even", "Primtallet", "111111"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "222222"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "333333"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "444444"));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "555555"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "666666"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "777777"));
        emergency.addPatient(new Patient("Inga", "Lykke", "888888"));
        emergency.addPatient(new Patient("Ulrik", "Smål", "999999"));
        hospital.getDepartments().put(emergency.getDepartmentName(), emergency);

        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "101010"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", "121212"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "131313"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "141414"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "151515"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "161616"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", "171717"));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", "181818"));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", "191919"));
        hospital.getDepartments().put(childrenPolyclinic.getDepartmentName(), childrenPolyclinic);
    }
}
